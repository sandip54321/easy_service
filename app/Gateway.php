<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gateway extends Model
{
    protected $table = 'gateways';

    protected $fillable = ['username','hostname','password','smscid',
                            'operator_id', 'gwname','port','priority'];


public function operator()
{
    return $this->belongsTo(Operator::class,'operator_id','id');
}

}


