<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Permission;
use Mail;


class AuthController extends Controller
{

     public function register(Request $request)
    {
        $rules=
             [
                'name' => 'required|regex:/^[\pL\s\-]+$/u|max:255',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|confirmed|min:6',
             ];
        $input = $request->only(
            'name',
            'email',
            'password',
            'password_confirmation'

        );

        $validator = Validator::make($input, $rules);

        if($validator->fails()) {
            $error = $validator->messages()->toJson();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->full_name = $request->full_name;
        $user->organization_name = $request->organization_name;
        $user->address = $request->address;
        $user->password = bcrypt($request->password);
        $user->phone_number = $request->phone_number; 
        $user->mobile_number = $request->mobile_number;
        $user->country = $request->country;  
        $user->user_type = $request->user_type; 
        $user->balance_type = $request->balance_type; 
        $user->creator_id = $request->creator_id;
        $verification_code = str_random(30);
        $user->verification_token = $verification_code;
        
        $user->save();
        
        
        $subject = "Please verify your email address.";
        Mail::send('emails.test', ['verification_code' => $verification_code],
        function($mail) use ($subject){
            $mail->from('easyservice@admin.com');
            $mail->to('client@gmaail.com');
            $mail->subject($subject);
        });
        return response([
            'status' => 'user succesfully registered ,click the link in email to verify registrstion',
        ], 200);
    }

    public function list_users($userid)
    {
       $user = User::where('creator_id',$userid)->get();
       return response([
           'status' => 'success',
           'data'  => $user
       ], 200); 
    }
    public function authenticate(Request $request)
    {
        // grab credentials from the request
            $credentials = [
                'email' => $request->email,
                'password' => $request->password,
                'is_verified' => 1
            ];
            
        
            try {
                // attempt to verify the credentials and create a token for the user

                if (! $token = JWTAuth::attempt($credentials)) {
                    return response()->json(['error' => 'invalid_credentials.Please make sure you entered the right information and you have verified your email address.'], 401);
                }
                    } catch (JWTException $e) {
                        // something went wrong whilst attempting to encode the token
                        return response()->json(['error' => 'could_not_create_token'], 500);
                    }
            // all good so return the token
            return response()->json(compact('token'));
             
    }
    public function verify_user($verification_code)
    {
        $check = User::where('verification_token',$verification_code)->first();
         if(!is_null($check)){

            if($check->is_verified == 1){
                return response()->json([
                    'success'=> true,
                    'message'=> 'Account already verified..'
                ]);
            }
           

           $check->update([
               'email_verified_at' => date('Y-m-d H:i:s'),
               'is_verified' => 1
               
               ]);
            
            return response()->json('account successfully verified');
           


        }

        return response()->json(['success'=> false, 'error'=> "Verification code is invalid."]);

    }

    public function updateUser(Request $request, $id){
        $user = User::find($id);
        try {
                 if (! $user = JWTAuth::parseToken()->authenticate())
                {
                 return response()->json(['user_not_found'], 404);
                }
            }
            catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                 return response()->json(['token_expired'], $e->getStatusCode());
             }
        $user->update([
            'name' => $request->name,
            'password' => bcrypt($request->password)
            ]);
         return response([
            'status' => 'update success',
            'data' => $user
          ], 200);

    }
    public function deleteUser($id){
        $user = User::find($id);
        $user->delete();
        return response(['status' => 'deleted succesfully'],200);
    }

    public function logout(Request $request) {
        $this->validate($request, ['token' => 'required']);

        try {
            JWTAuth::invalidate($request->input('token'));
            return response([
            'status' => 'success',
            'msg' => 'You have successfully logged out.'
        ]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response([
                'status' => 'error',
                'msg' => 'Failed to logout, please try again.'
            ]);
        }
    }
     public function getallusers()
     {
         $user = User::all();
         return response()->json(compact('user'));
     }
    public function getAuthenticatedUser()
            {
                    try {

                            if (! $user = JWTAuth::parseToken()->authenticate()) {
                                    return response()->json(['user_not_found'], 404);
                            }

                                } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                                        return response()->json(['token_expired'], $e->getStatusCode());

                                } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                                        return response()->json(['token_invalid'], $e->getStatusCode());

                                } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                                        return response()->json(['token_absent'], $e->getStatusCode());

                                }

                    return response()->json(compact('user'));
            }

    public function refresh()
    {
        return response([
            'status' => 'success'
        ]);
    }
    public function createRole(Request $request){

        $role = new Role();
        $role->name = $request->input('name');
       // dd($role);
        $role->save();

        return response()->json("created");

    }
     public function createPermission(Request $request){

        $viewUsers = new Permission();
        $viewUsers->name = $request->input('name');
        $viewUsers->save();

        return response()->json("created");

    }
    public function assignRole(Request $request){
        $user = User::where('email', '=', $request->input('email'))->first();

        $role = Role::where('name', '=', $request->input('role'))->first();
        //$user->attachRole($request->input('role'));
        $user->roles()->attach($role->id);

        return response()->json("role assigned to user");
    }
    public function attachPermission(Request $request){
        $role = Role::where('name', '=', $request->input('role'))->first();
        $permission = Permission::where('name', '=', $request->input('permission'))->first();
        $role->attachPermission($permission);

        return response()->json(" permissions attached to role");
    }
}
