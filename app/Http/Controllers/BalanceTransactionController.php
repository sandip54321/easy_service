<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BalanceTransaction;
use App\User;
use App\UserBalance;

class BalanceTransactionController extends Controller
{
    public function balanceaccountopen(Request $request)
    {
      $balance = new UserBalance;
      $balance->user_id = $request->user_id;
      $amount = $request->input('balance');
      $unitrate = $request->input('unitrate');
      $balance->balance_type = $request->balance_type;
      $balance->description = $request->description;
      $unit = $request->input('unit');
      if(isset($unit)){
        $balance->unit = $unit;
       }
      else{
        $unitconversion = intval($amount/$unitrate);
        $balance->unit = $unitconversion;
        $balance->unitrate = $unitrate;
      }
      $balance->save();
      return response()->json($balance, 201);
    }


    public function addbalance(Request $request,UserBalance $balance)
    {

      $unit = $balance->unit;
      $newunit = $request->input('unit');
      $description = $request->input('description');
      $amount = $request->input('balance');
      if(isset($newunit))
      {
          $balance->update([
            'description'=>$description,
            'unit'=>($unit + $newunit)
           ]);

      }
      else{
        $unitrate = $balance->unitrate;
        if(!isset($unitrate))
        {
          return "unitrate not given";
        }
        $unitconversion = intval($amount/$unitrate);
        $balance->update([
          'description'=>$description,
          'unit'=>($unit + $unitconversion)
         ]);
      }
       return $balance;
    }

    public function updateunitrate(Request $request,UserBalance $balance)
    {
      $balance->update([
          'unitrate'=>$request->unitrate
      ]);
      return $balance;
    }
    public function checkbalance($id)
    {
      $user = User::find($id);
      $balance = UserBalance::where('user_id',$id)->get();
      if(!$balance){
        return "balance account not available";
      }
      return response()->json($balance,201);
    }

}
