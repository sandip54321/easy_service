<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gateway;
use App\Operator;

class GatewayController extends Controller
{

    public function index()
    {
        $gateway = Gateway::with('operator')->get();
        if($gateway->isEmpty()){
            return "no gateway";
        }
       
        return response()->json($gateway, 201);
    }

    public function store(Request $request)
    {
        $gateway = Gateway::create($request->all());

        return response()->json($gateway, 201);
    }

    public function show($id)
    {
      if($gateway = Gateway::find($id)){
        return response()->json($gateway, 201);
      }
        return "not found";   
    }

    public function update(Gateway $gateway,Request $request)
    {
        $gateway->update($request->all());

        return response()->json($gateway, 200);
    }

    public function destroy($id)
    {
        if($gateway = Gateway::find($id))
        {
          $gateway->delete();
          return "successfully deleted";
            
        }
        return "not found";
    }
}
