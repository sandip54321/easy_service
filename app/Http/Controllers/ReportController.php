<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SendSms;
use App\SenderId;
use App\User;
use App\Operator;


class ReportController extends Controller
{
    public function reportby_Id($id)
    {
      $sms = SendSms::find($id);
      return response()->json(["success"=>"true","data"=> $sms]);
    }
    public function reportby_senderId($id)
    {
      //$senderid = SenderId::find($id);
      $sms = SendSms::with('senderId')->where('senderid_id',$id)->get();
      return response()->json($sms,201);
    }
     public function smsreport_by_user($id)
     {
       $senderid = SenderId::with('users')->where('user_id',$id)->get()->toArray();
      // return $senderid;
       //$count = count($senderid);
          $sms = SendSms::with('senderId')->where('senderid_id',$senderid)->get();
          return $sms;

      }

      public function smsreport_by_operator($id)
      {
         $senderid = SenderId::with('operators')->where('operator_id',$id)->get()->toArray();
          $sms = SendSms::with('senderId')->where('senderid_id',$senderid)->get();
          return $sms;

      }

      public function  smsreport_by_daterange(Request $request)
      {
        $startdate = date('y-m-d', strtotime($request->input('startdate')));
        $lastdate = date('y-m-d' ,strtotime($request->input('lastdate')));
      //  return $startdate.$lastdate;
         $sms = SendSms::whereBetween('created_at',[$startdate,$lastdate])->get();
        return response()->json(["success"=>"true",
                                  "data"=>$sms]);

      }







}
