<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Send_push;
use App\Operator;
use App\SenderId;
use App\User;
use App\ContactList;
use App\BalanceTransaction;
use App\ContactNumber;
use App\UserBalance;


class SendPushController extends Controller
{
    public function sendSms(Request $request)
    {
      //fetch contact number from contactlist
      $contactlist = $request->input('contactlist');
      $listid = ContactNumber::where('contactlist_id',$contactlist)->pluck('phone_number')->toArray();
      //input phone numbers
      $number = $request->input('number');
      $str_array = explode(",",$number);
       //$str_array = implode(" ",$str_num);
      //merge contact number from contact list and directly inputted number
       $total_numbers = array_merge($listid,$str_array);
       //check number validity
      $results = array_unique($total_numbers);
      $str_size = sizeof($results);
      foreach($results as $result){
        if(strlen($result) != 10){
          $invalid_numbers[] = $result;
        }
      }
       //valid numbers only
      $final_number = array_diff($results,$invalid_numbers);
       //return $final_number;
      //fetch operator
      //$operatorid = $request->input('smsc_id');
      //$operator = Operator::where('id',$operatorid)->pluck('operator_name');
       //if($operator->isEmpty())
      // {
        // return "operator not found";
       //}
      //fetch username
      $senderid = $request->input('sender');
      $sender = User::where('id',$senderid)->first();
      if(!$sender)
       {
         return "sender not found";
       }
       $sendername = $sender->name;
      //fetch senderid
        $userid = $request->input('service');
        foreach($userid as $username)
        {
          $user[] = SenderId::where('id',$username)->first();
        }
  
        if(empty($user[0]))
       {
        return "service not found";
       }
        foreach($user as $service)
        {
          $servicename[] = $service->senderid;
        }
       //message type
       $msgtype = $request->input('msgtype');
       $msg = $request->input('msgdata');
       if($msgtype == 1 || $msgtype == 3)
        {
          $msglength = strlen($msg);
          $totalcount = 0;
          if($msglength <= 160)
          {
            $totalcount = 1;
          }
          elseif ($msglength > 160 )
            {
              $msgcount = ($msglength / 153);
              $intmsgcount = intval($msgcount);
              if($msgcount > $intmsgcount)
              {
              $totalcount = ++$intmsgcount;
              }
              else
                {
                $totalcount = $intmsgcount ;
                }
            }
            $coding = 0;
            $charset = NULL;
        }
        elseif($msgtype == 2 || $msgtype == 4)
        {
        $unimsg =  mb_convert_encoding($msg, "UTF-16BE");
          //mb_strlen($string, "UTF-16BE")
        $msglength = mb_strlen($unimsg, "UTF-16BE");
        $totalcount = 0;
        if($msglength <= 70)
        {
          $totalcount = 1;
        }
        elseif ($msglength > 70 )
          {
            $msgcount = ($msglength / 67);
            $intmsgcount = intval($msgcount);
            if($msgcount > $intmsgcount)
            {
            $totalcount = ++$intmsgcount;
            }
            else
              {
              $totalcount = $intmsgcount ;
              }

          }
        $coding = 2;
        $charset = "UTF-16BE";

        }
        else{
          return "encoding not found";
        }

       //$totalbalance = array_sum($balance);
       //$size = sizeof($results);
       //$chars = str_split($str_array,10);
       //return $chars;
      foreach ($final_number  as $result)
      {
          $send_push = new Send_push();
          //check balance
          $balance = UserBalance::where('user_id',$senderid)->get();
         if (preg_match("/^984|^985|^986|^974|^975/",$result))
           {
            $balancenamaste =  UserBalance::where('balance_type',$balance->balance_type=1)->first();

            if($balancenamaste->unit <= 1)
            {
               return "Nepal Telecom balance not sufficient ";
            }

           }
           if(preg_match("/^980|^981|^982/",$result))
           {
             $balanceaxiata =  UserBalance::where('balance_type',$balance->balance_type=2)->first();
             if($balanceaxiata->unit <=1)
             {
               return "Ncell Balance  not sufficient";
             }
           }
          $send_push->momt = $request->momt;
          $send_push->mwi = $request->mwi;
          $send_push->compress = $request->compress;
          $send_push->validity = $request->validity;
          $send_push->deferred = $request->deferred;
          $send_push->dlr_mask = $request->dlr_mask;
          $send_push->dlr_url = $request->dlr_url;
          $send_push->pid = $request->pid;
           if(preg_match("/^984|^985|^986|^974|^975/",$result)){
            foreach($userid as $username)
            {
              $user[] = SenderId::where('id',$username)->first();
              foreach($user as $senderuser)
             {
              if($senderuser->operator_id == 3)
              {
                $operator = Operator::where('id',3)->pluck('operator_name');
                $send_push->service = $senderuser->senderid;
                $send_push->smsc_id = $operator;
              }
             }
            }

           }
           if(preg_match("/^980|^981|^982/",$result)){
            foreach($userid as $username)
            {
              $user[] = SenderId::where('id',$username)->first();
              foreach($user as $senderuser)
              {
                if($senderuser->operator_id == 2)
                {
                  $operator = Operator::where('id',2)->pluck('operator_name');
                  $send_push->service = $senderuser->senderid;
                  $send_push->smsc_id = $operator;
                }
              }

            }

           }
          $send_push->alt_dcs = $request->alt_dcs;
          $send_push->rpi = $request->rpi;
          $send_push->boxc_id = $request->boxc_id;
          $send_push->binfo = $request->binfo;
          $send_push->meta_data = $request->meta_data;
          $send_push->receiver = $result;
          $send_push->sender = $sendername;
          $send_push->msgdata = $msg;
          $send_push->time = time();
          $send_push->sms_type = $request->sms_type;
          $send_push->mclass = $request->mclass;
          $send_push->coding = $coding;
          $send_push->charset = $charset;
            //update balance
             if(preg_match("/^984|^985|^986|^974|^975/",$result))
             {
              $newbalancentc = ($balancenamaste->unit - ($totalcount * 1));
             }
             if(preg_match("/^980|^981|^982/",$result)){
              $newbalancencell =($balanceaxiata->unit - ($totalcount * 1));
             }
             if(preg_match("/^984|^985|^986|^974|^975/",$result)){
              $balancenamaste->update([
              'unit' => $newbalancentc
              ]);
             }
             if(preg_match("/^980|^981|^982/",$result)){
              $balanceaxiata->update([
                'unit' => $newbalancencell
              ]);
             }

          //inserting in balance_transaction table
          $balance_transaction = new BalanceTransaction;
          $balance_transaction->user_id = $senderid;
          $balance_transaction->transaction_type = 2;
          $balance_transaction->transaction_descrption = 'deduced : Balance deduced due to Sending SMS';
          if (preg_match("/^984|^985|^986|^974|^975/",$result))
          {
          $balance_transaction->balance_type = 3;
          $balance_transaction->balance_amount = $totalcount;
          $balance_transaction->balance_after_update = ($balancenamaste->unit -$totalcount);
          }
          if (preg_match("/^980|^981|^982/",$result))
          {
          $balance_transaction->balance_type = 2;
          $balance_transaction->balance_amount = $totalcount;
          $balance_transaction->balance_after_update = ($balanceaxiata->unit -$totalcount);
          }
          $balance_transaction->save();

          //inserting in outbox table


          $send_push->save();

        $data[] = $balance_transaction;

        }
          return response()->json($data,201);
    }


}
