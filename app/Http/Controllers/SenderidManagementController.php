<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SenderId;
//use App\Operator;
//use App\User;

class SenderidManagementController extends Controller
{
    public function addSenderid(Request $request)
    {
        $senderid = new SenderID();
        $senderid->senderid = $request->senderid;
        $senderid->user_id = $request->user_id;
        $senderid->operator_id = $request->operator_id;
        $senderid->gateway_id = $request->gateway_id;
        $senderid->descriptions = $request->descriptions;
        $senderid->default = $request->default;
       
        $senderid->status = 1;
        $senderid->save();
         return response()->json($senderid,201);
    }

    public function index()
    {
        $senderid = SenderId::with('operators')->with('gateways')->get();
        if($senderid->isEmpty()){
            return "no senderID requested";
        }

        return response()->json($senderid, 201);
    }

    public function approveSenderid($id)
    {
        $senderid = SenderId::findorFail($id);
        if($senderid->status == 2)
        {
            return "already approved";
        }
        elseif($senderid->status == 1 || 3)
        {
            $senderid->update(['status' => 2]);
            return response()->json($senderid,201);
        }
        
       

    }

    public function disapproveId(Request $request,$id)
    {
        $senderid = SenderId::findorFail($id);
        if($senderid->status == 1 || 2)
        {
            $senderid->update(['status' => 3]);
            return response()->json($senderid,201);
        }
    }

    
    public function showbyId($id)
    {
        $senderid = SenderId::with('operators')->with('gateways')->find($id);
        return response()->json($senderid,201);
    }
}
