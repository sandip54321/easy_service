<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Send_push;
use App\Operator;
use App\SenderId;
use App\User;
use App\ContactList;
use App\BalanceTransaction;
use App\ContactNumber;
use App\UserBalance;
use App\SmsTransaction;

class TestSendPushController extends Controller
{
    public function sendPush(Request $request)
    {
      $contact = $request->input('contact');
      $number = $request->input('number');
      $validnumbers = $this->getContact($contact,$number);
      $sizeofnumbers = count($validnumbers);
      $account = $request->input('account');
      $getaccount = $this->getAccount($account);
       //input senderid
      $senderid = $request->get('sender');
     // $sender = $this->getSender($validnumbers,$senderid);
      //message type
      $msgtype = $request->input('msgtype');
      $msg = $request->input('msgdata');
      $countmessage = $this->countMessage($msgtype,$msg);
      $balance = UserBalance::where('user_id',$account)->get();
      //get balance
      $getbalance = $this->getBalance($balance);

      //checking balance
      $checkbalance = $this->checkBalance($getbalance,$validnumbers,$countmessage);
      //updating balance
      $updatebalance = $this->updateBalance($checkbalance,$getbalance);
      if (array_key_exists("balancentc",$checkbalance) || array_key_exists("balancencell",$checkbalance))
      {
        return $updatebalance;
      }

      //insert into balance_transaction table
      foreach($updatebalance as $separate)
      {

        if($separate->balance_type == 1)
        {
          $res['balance_type'] = $separate['balance_type'];
          $res['balance_amount'] = $checkbalance['ntc'];
          $res['balance_after_update'] = $separate['unit'];

        }
        elseif($separate->balance_type == 2)
        {
          $res['balance_type'] = $separate['balance_type'];
          $res['balance_amount'] = $checkbalance['ncell'];
          $res['balance_after_update'] = $separate['unit'];
        }
         DB::table('balance_transactions')->insert(
            array(
              'user_id' => $account,
              'transaction_type' => 2,
              'transaction_descrption' => "balance deduced due to sending sms",
              'balance_type' => $res['balance_type'],
              'balance_after_update' => $res['balance_after_update'],
              'balance_amount' => $res['balance_amount'],
              'created_at' => date("Y-m-d H:i:s")
            )
          );

      }
      //insert into sms_transactions table
      foreach($validnumbers as $eachnumber)
      {
        $sender = $this->getSender($senderid,$eachnumber);
          $res['sender'] = $sender['sender'];
          $res['msgcount'] = $countmessage['character'];
          $res['msg_units'] = $countmessage['totalcount'];
          $res['cell_no']  = $sender['operator'];

        DB::table('sms_transactions')->insert(
          array(

                'user_id' => $account,
                'sender' =>  $res['sender'],
                'message' => $msg,
                'msgtype' => $msgtype,
                'msgcount' => $res['msgcount'],
                'msg_units' => $res['msg_units'],
                'cell_no' =>  $res['cell_no'].":1",
                'created_at' => date("Y-m-d H:i:s")

                )
          );
      }
      //insert into outbox table
     foreach($validnumbers as $eachnumber)
     {
      $sender = $this->getSender($senderid,$eachnumber);
         DB::table('outbox')->insert(
          array(
            'mobile_number' => $eachnumber,
            'operator' => $sender['operator_id'],
            'status' => 1,
            'admin_state' => 0,
            'created_at' => date("Y-m-d H:i:s")
          )
          );
     }

      //preparing array for insertinfg into send_push table

      foreach($validnumbers as $eachnumber)
      {
        $sender = $this->getSender($senderid,$eachnumber);
         //return $sender;
        //$service = $this->getService($senderid,$eachnumber);
        $result['sender'] = $sender['sender'];
        $result['service']  = $sender['service'];
        $result['smsc_id']  = $sender['smsc_id'];
        $result['coding'] =  $countmessage['coding'];
        $result['charset'] = $countmessage['charset'];
        $result['mclass'] = $countmessage['mclass'];
        $result['msgdata'] = $msg;
        $result['sms_type'] = $msgtype;
        $result['receiver']  = $eachnumber;
        $result['momt'] = 'MT';
        $result['dlr_mask'] = 31;
        //$data['totalcount'] = $countmessage['totalcount'];
        //$data['newbalance'] = $checkbalance;
        $sendpush[] = $result;
      //  $outbox[] = $data;


      }

      //checking and updating balance
      //  return $sendpush;
      $results = array_chunk($sendpush,2);
      //return $results;
      //saving to send_push
      foreach($results as $value)
      {
        DB::table('send_push')->insert($value);
      }
        return response()->json($results,201);
    }

    public function getContact($contact,$number)
    {
      $res = array();
      $listid = ContactNumber::where('contactlist_id',$contact)->pluck('phone_number')->toArray();
      $str_array = explode(",",$number);
      $total_numbers = array_merge($listid,$str_array);
      $results = array_unique($total_numbers);
      $invalid_numbers = array();
      foreach($results as $result){
        if(strlen($result) != 10){
          $invalid_numbers[] = $result;
        }
      }
       $final_number = array_diff($results,$invalid_numbers);
        //$res['finalnumbers'] = $final_number;
        //$res['invalidnumbers']  = $invalid_numbers;
        return $final_number;

    }

    public function getAccount($account)
    {
      $account = User::where('id',$account)->first();
      if(!$account)
       {
         return "user not found";
       }
       $username = $account->name;
       return $username;
    }

    public function getSender($senderid,$eachnumber)
    {
      foreach($senderid as $username)
      {
        $sendername[] = SenderId::where('id',$username)->first();
      }
      $res = array();
      if(preg_match("/^984|^985|^986|^974|^975/",$eachnumber))
      {
        foreach($sendername as $senderuser)
        {
          //return $senderuser->operator_id;
          if($senderuser->operator_id == 3)
              {
                $res['sender']= $senderuser->senderid;
                $res['smsc_id'] = $senderuser->gateways->smscid;
                $res['service'] = $senderuser->gateways->username;
                $res['operator'] = $senderuser->operators->operator_name;
                $res['operator_id'] = $senderuser->operators->id;
              }
        }
      }
      if(preg_match("/^980|^981|^982/",$eachnumber))
      {
        foreach($sendername as $senderuser)
        {
          //return $senderuser->operator_id;
          if($senderuser->operator_id == 2)
              {

                $res['sender']= $senderuser->senderid;
                $res['smsc_id'] = $senderuser->gateways->smscid;
                $res['service'] = $senderuser->gateways->username;
                $res['operator'] = $senderuser->operators->operator_name;
                $res['operator_id'] = $senderuser->operators->id;
              }
        }
      }
      return $res;
    }
    public function countMessage($msgtype,$msg)
    {
      $res = array();
      if($msgtype == 1 || $msgtype == 3)
      {
        $msglength = strlen($msg);
        $totalcount = 0;
        if($msglength <= 160)
        {
          $totalcount = 1;
        }
        elseif ($msglength > 160 )
        {
            $msgcount = ($msglength / 153);
            $intmsgcount = intval($msgcount);
            if($msgcount > $intmsgcount)
            {
            $totalcount = ++$intmsgcount;
            }
            else
            {
              $totalcount = $intmsgcount ;
            }
        }
        $coding = 0;
        $charset =NULL ;
      }
      elseif($msgtype == 2 || $msgtype == 4)
      {
        $unimsg =  mb_convert_encoding($msg, "UTF-16BE");
        $msglength = mb_strlen($unimsg, "UTF-16BE");
        $totalcount = 0;
        if($msglength <= 70)
        {
          $totalcount = 1;
        }
        elseif ($msglength > 70 )
        {
            $msgcount = ($msglength / 67);
            $intmsgcount = intval($msgcount);
            if($msgcount > $intmsgcount)
            {
            $totalcount = ++$intmsgcount;
            }
            else
            {
              $totalcount = $intmsgcount ;
            }

        }
        $coding = 2;
        $charset = "UTF-16BE";

      }
      else
      {
       return "encoding not found";
      }
      if($msgtype == 3 || $msgtype == 4)
      {
        $mclass = 0;
      }
      else{
        $mclass = NULL;
      }
      //return $totalcount;
      $res['totalcount'] = $totalcount;
      $res['coding'] = $coding;
      $res['charset'] = $charset;
      $res['mclass'] =  $mclass;
      $res['character'] = $msglength;
      return $res;
    }

    public function getBalance($balance)
    {
       $res = array();
       foreach($balance as $single)
       {
         if($single->balance_type == 1)
         {
           $res['ntc'] = $single;
         }
         elseif($single->balance_type == 2)
         {
           $res['ncell']  = $single;
         }
       }
       return $res;
    }
    public function checkBalance($getbalance,$validnumbers,$countmessage)
    {

      $ntcnumbers = preg_grep("/^984|^985|^986|^974|^975/",$validnumbers);
      $Ncellnumbers = preg_grep("/^980|^981|^982/",$validnumbers);
      if($ntcnumbers)
      {
        $countnum['ntc'] = count($ntcnumbers);
        $unitrequire['ntc'] = $countnum['ntc'] * $countmessage['totalcount'];
        $balance['ntc'] = $getbalance['ntc']->unit;
        if($balance['ntc'] < $unitrequire['ntc'])
          {

            $res['balancentc'] = "ntc balance not sufficient";

          }
         else
         {
          $res['ntc'] = $unitrequire['ntc'];
         }

      }
      if($Ncellnumbers)
      {

        $countnum['ncell'] =count($Ncellnumbers);
        $unitrequire['ncell'] = $countnum['ncell'] * $countmessage['totalcount'];
        $balance['ncell'] = $getbalance['ncell']->unit;
        if($balance['ncell'] < $unitrequire['ncell'])
          {
            $res['balancencell'] = "ncell balance not sufficient";

          }
          else
          {
            $res['ncell'] = $unitrequire['ncell'];
          }


      }

       return $res;
    }

    public function updateBalance($checkbalance,$getbalance)
    {
      if (array_key_exists("balancentc",$checkbalance))
      {
        return $checkbalance['balancentc'];

      }
      if (array_key_exists("balancencell",$checkbalance))
      {
        return $checkbalance['balancencell'];

      }
      if (array_key_exists("ntc",$checkbalance))
      {
        $updatedbalance['ntc'] = ($getbalance['ntc']->unit - $checkbalance['ntc']);
        $getbalance['ntc']->update([
            'unit' => $updatedbalance['ntc']
          ]);
        $newbalance['ntc'] = $getbalance['ntc'];

      }
      if (array_key_exists("ncell",$checkbalance))
      {
        $updatedbalance['ncell'] = ($getbalance['ncell']->unit - $checkbalance['ncell']);
        $getbalance['ncell']->update([
        'unit' => $updatedbalance['ncell']
        ]);
        $newbalance['ncell'] = $getbalance['ncell'];
      }
        return $newbalance;

    }


}
