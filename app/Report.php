<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public function reportby_senderIds()
    {
      return $this->belongsTo('App/SenderId');
    }
    public function reportby_users()
    {
      return $this->belongsTo('App/User');
    }
    public function reportby_operators()
    {
      return $this->belongsTo('App/Operator');
    }
    
}
