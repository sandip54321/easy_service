<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendSms extends Model
{
    protected $table = 'send_sms';
    protected $fillable = ['senderid_id','contactlist_id','contactnumbers_id','number','body'];

    public function senderId()
    {
        return $this->belongsTo('App\SenderId','senderid_id','id');

    }

    public function contactlists()
    {
        return $this->hasMany('App\ContactList','id','contactlist_id');
    }

    public function contactnumbers()
    {
        return $this->hasMany('App\ContactNumber');
    }
    public function balance()
    {
      return $this->hasOne('App\BalanceTransaction');
    }

    public function user_report()
    {
      return $this->belongsTo('App\User');
    }

}
