<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Send_push extends Model
{
    protected $table = 'send_push';
    public $timestamps = false;
    protected $fillable = ['sql_id','momt','sender','receiver','udhdata','msgdata','time','smsc_id',
                            'service','account','id','sms_type','mclass','mwi','coding','compress','validity',
                            'deferred','dlr_mask','dlr_url','pid','alt_dcs','rpi','charset','boxc_id','binfo',
                            'meta_data','foreign_id'];

                            
}
