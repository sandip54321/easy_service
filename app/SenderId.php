<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SenderId extends Model
{
    protected $table = 'easy_senderids';
    protected $fillable = ['senderid','user_id','status','operator_id','gateway_id','descriptions','already_exists','default'];

    public function operators()
    {
        return $this->belongsTo('App\Operator','operator_id','id');
    }
    public function gateways()
    {
        return $this->belongsTo('App\Gateway','gateway_id','id');
    }

    public function users()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function sms()
    {
      return $this->hasMany('App\SendSms');
    }



}
