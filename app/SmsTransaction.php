<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsTransaction extends Model
{
    protected $table = 'sms_transactions';

    protected $fillable =['user_id','sender','message',
                          'msgtype','msgcount','cell_no',
                           'msg_units'];

}
