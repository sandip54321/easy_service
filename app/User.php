<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable  implements JWTSubject
{
    use Notifiable;
    use EntrustUserTrait;

    protected $table = 'users';
    
    protected $fillable = [
        'name', 'email', 'password','is_verified','email_verified_at'
    ];

    
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
      return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
      return [];
    }

    public function senderid()
    {

      return $this->hasMany('App\SenderId');
    }
}
