<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{
    protected $table = 'users_balance';

    protected $fillable = ['user_id','amount','balance_type','description','unit','unitrate'];

    public function user_balance()
  {
    return $this->belongsTo(User::class,'user_id','id');
  }
    
}
