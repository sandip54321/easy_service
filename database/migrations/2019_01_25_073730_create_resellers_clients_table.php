<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResellersClientsTable extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('full_name');
            $table->string('organization_name')->nullable();
            $table->string('address');
            $table->string('phone_number');
            $table->string('mobile_number')->nullable();
            $table->string('country')->nullable();
            $table->enum('user_type',['1=admin','2=reseller','3=client'])->nullable();
            $table->enum('balance_type',['1=single','2=separated'])->nullable();
            $table->integer('creator_id')->nullable();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
