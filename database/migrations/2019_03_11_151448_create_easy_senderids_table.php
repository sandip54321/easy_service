<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEasySenderidsTable extends Migration
{
    
    public function up()
    {
        Schema::create('easy_senderids', function (Blueprint $table) {
            $table->increments('id');
            $table->string('senderid');
            $table->integer('user_id')->unsigned();
            $table->integer('status');
            $table->integer('gateway_id')->unsigned();
            $table->integer('operator_id')->unsigned();
            $table->integer('default');
            $table->integer('already_exists')->default(0);
            $table->string('descriptions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('easy_senderids');
    }
}
