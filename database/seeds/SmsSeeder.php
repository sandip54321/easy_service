<?php

use Illuminate\Database\Seeder;
use App\Send_push;

class SmsSeeder extends Seeder
{
    
    public function run()
    {
        $sms = [
            [
                'momt' => 'MT',
                'sender' => 'easysms',
                'receiver' => '7464457,85757,847585',
                'udhdata'=>NULL,
                'msgdata' =>'helllo ,this is your first sms',
                'time' => NULL,
                'smsc_id' => '1',
                'service' => 'easysms',
                'account' => 'separated',
                'id' => NULL,
                'sms_type' =>'1',
                'mclass' =>NULL,
                'mwi' => NULL,
                'coding'=> '1',
                'compress' => NULL,
                'validity' => NULL,
                'deferred' => NULL,
                'dlr_mask' => NULL,
                'dlr_url' => NULL,
                'pid' => NULL,
                'alt_dcs' => NULL,
                'rpi' => NULL,
                'charset' => NULL,
                'boxc_id' => NULL,
                'binfo' => NULL,
                'meta_data'=> NULL,
                'foreign_id' =>NULL
            ]

        ];
           foreach($sms as $value)
           {

                 Send_push::create($value);
           }
        
       
    }
}
