-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2019 at 02:59 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `easy_service`
--

-- --------------------------------------------------------

--
-- Table structure for table `balance_transactions`
--

CREATE TABLE `balance_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `transaction_type` enum('1=alloted','2=deduced') COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_descrption` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `balance_after_update` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `balance_transactions`
--

INSERT INTO `balance_transactions` (`id`, `user_id`, `transaction_type`, `transaction_descrption`, `balance_type`, `balance_amount`, `balance_after_update`, `created_at`, `updated_at`) VALUES
(76, 13, '2=deduced', 'balance deduced due to sending sms', '1', '6', '14', '2019-03-23 16:37:24', NULL),
(77, 13, '2=deduced', 'balance deduced due to sending sms', '2', '4', '26', '2019-03-23 16:37:24', NULL),
(78, 13, '2=deduced', 'balance deduced due to sending sms', '1', '6', '8', '2019-03-23 16:41:20', NULL),
(79, 13, '2=deduced', 'balance deduced due to sending sms', '2', '4', '22', '2019-03-23 16:41:20', NULL),
(80, 13, '2=deduced', 'balance deduced due to sending sms', '1', '6', '2', '2019-03-23 16:48:13', NULL),
(81, 13, '2=deduced', 'balance deduced due to sending sms', '2', '4', '18', '2019-03-23 16:48:13', NULL),
(82, 13, '2=deduced', 'balance deduced due to sending sms', '1', '6', '14', '2019-03-23 16:52:39', NULL),
(83, 13, '2=deduced', 'balance deduced due to sending sms', '2', '4', '14', '2019-03-23 16:52:39', NULL),
(84, 13, '2=deduced', 'balance deduced due to sending sms', '1', '6', '8', '2019-03-23 16:54:34', NULL),
(85, 13, '2=deduced', 'balance deduced due to sending sms', '2', '4', '10', '2019-03-23 16:54:34', NULL),
(86, 13, '2=deduced', 'balance deduced due to sending sms', '1', '6', '2', '2019-04-12 09:05:32', NULL),
(87, 13, '2=deduced', 'balance deduced due to sending sms', '2', '4', '6', '2019-04-12 09:05:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact_lists`
--

CREATE TABLE `contact_lists` (
  `id` int(10) UNSIGNED NOT NULL,
  `listname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_lists`
--

INSERT INTO `contact_lists` (`id`, `listname`, `description`, `created_at`, `updated_at`) VALUES
(1, 'sandip', 'all', '2018-11-28 05:23:49', '2018-11-28 05:23:49'),
(2, 'civilbank', 'contact lists of civil bank', '2018-12-03 02:18:14', '2018-12-03 02:18:14'),
(3, 'abcbank', 'contact lists of civil bank', '2019-01-16 05:12:02', '2019-01-16 05:12:02'),
(4, 'easy family', 'contact lists of easy service', '2019-02-18 05:55:37', '2019-02-18 05:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `contact_numbers`
--

CREATE TABLE `contact_numbers` (
  `id` int(10) UNSIGNED NOT NULL,
  `contactlist_id` int(11) NOT NULL,
  `phone_number` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_numbers`
--

INSERT INTO `contact_numbers` (`id`, `contactlist_id`, `phone_number`, `username`, `created_at`, `updated_at`) VALUES
(1, 1, '987654321', 'kanchanbhattarai', '2018-11-29 03:19:08', '2018-11-29 03:19:08'),
(14, 1, '986574365456', 'bibek', '2018-12-01 09:57:26', '2018-12-01 09:57:26'),
(15, 1, '846646464', NULL, '2018-12-01 10:06:31', '2018-12-01 10:06:31'),
(16, 1, '775675675', NULL, '2018-12-01 10:06:32', '2018-12-01 10:06:32'),
(17, 1, '9843298765', NULL, '2018-12-01 10:19:23', '2018-12-01 10:19:23'),
(18, 1, '9876543211', NULL, '2018-12-01 10:19:23', '2018-12-01 10:19:23'),
(19, 1, '743547', NULL, '2018-12-01 10:24:31', '2018-12-01 10:24:31'),
(20, 1, '8458588', NULL, '2018-12-01 10:24:31', '2018-12-01 10:24:31'),
(21, 1, '7547', NULL, '2018-12-01 10:26:12', '2018-12-01 10:26:12'),
(22, 1, '8488', NULL, '2018-12-01 10:26:12', '2018-12-01 10:26:12'),
(23, 1, '7547', NULL, '2018-12-02 00:10:12', '2018-12-02 00:10:12'),
(24, 1, '8488', NULL, '2018-12-02 00:10:12', '2018-12-02 00:10:12'),
(25, 1, '75123447', NULL, '2018-12-02 00:11:33', '2018-12-02 00:11:33'),
(26, 1, '84323388', NULL, '2018-12-02 00:11:33', '2018-12-02 00:11:33'),
(27, 1, '75123447', NULL, '2018-12-02 00:12:50', '2018-12-02 00:12:50'),
(28, 1, '84323388', NULL, '2018-12-02 00:12:50', '2018-12-02 00:12:50'),
(29, 1, '75123447', NULL, '2018-12-02 00:13:08', '2018-12-02 00:13:08'),
(30, 1, '84323388.1111111', NULL, '2018-12-02 00:13:08', '2018-12-02 00:13:08'),
(31, 1, '75123447', NULL, '2018-12-02 00:13:24', '2018-12-02 00:13:24'),
(32, 1, '84323388', NULL, '2018-12-02 00:13:24', '2018-12-02 00:13:24'),
(33, 1, '1111111', NULL, '2018-12-02 00:13:24', '2018-12-02 00:13:24'),
(34, 1, '75123447', NULL, '2018-12-02 00:16:27', '2018-12-02 00:16:27'),
(35, 1, '84323388', NULL, '2018-12-02 00:16:27', '2018-12-02 00:16:27'),
(37, 1, '8432338', NULL, '2018-12-02 00:18:11', '2018-12-02 00:18:11'),
(38, 1, '9862087576', NULL, '2018-12-02 00:18:11', '2018-12-02 00:18:11'),
(39, 1, '9872094355', NULL, '2018-12-02 00:18:11', '2018-12-02 00:18:11'),
(40, 1, '7517', NULL, '2018-12-02 01:15:37', '2018-12-02 01:15:37'),
(41, 1, '32338', NULL, '2018-12-02 01:15:37', '2018-12-02 01:15:37'),
(42, 1, '986208', NULL, '2018-12-02 01:15:37', '2018-12-02 01:15:37'),
(43, 1, '9872095', NULL, '2018-12-02 01:15:37', '2018-12-02 01:15:37'),
(44, 1, '77', NULL, '2018-12-02 01:24:55', '2018-12-02 01:24:55'),
(45, 1, '38', NULL, '2018-12-02 01:24:55', '2018-12-02 01:24:55'),
(46, 1, '988', NULL, '2018-12-02 01:24:55', '2018-12-02 01:24:55'),
(47, 1, '9895', NULL, '2018-12-02 01:24:55', '2018-12-02 01:24:55'),
(48, 1, '7', NULL, '2018-12-02 01:27:38', '2018-12-02 01:27:38'),
(49, 1, '8', NULL, '2018-12-02 01:27:39', '2018-12-02 01:27:39'),
(50, 1, '88', NULL, '2018-12-02 01:27:39', '2018-12-02 01:27:39'),
(51, 1, '895', NULL, '2018-12-02 01:27:39', '2018-12-02 01:27:39'),
(52, 1, '70', NULL, '2018-12-02 01:40:36', '2018-12-02 01:40:36'),
(53, 1, '7000', NULL, '2018-12-02 01:55:33', '2018-12-02 01:55:33'),
(54, 1, '8000', NULL, '2018-12-02 01:55:34', '2018-12-02 01:55:34'),
(55, 1, '987654300', 'ram', NULL, NULL),
(56, 1, '987654300', 'ram', NULL, NULL),
(57, 1, '987654300', 'ram', NULL, NULL),
(58, 1, '12300', 'rame', '2018-12-02 04:27:22', '2018-12-02 04:27:22'),
(59, 1, '12300', 'rame', '2018-12-02 04:32:31', '2018-12-02 04:32:31'),
(60, 1, '43210', 'harie', '2018-12-02 04:32:31', '2018-12-02 04:32:31'),
(61, 1, '12300', 'rame', '2018-12-02 05:25:26', '2018-12-02 05:25:26'),
(62, 1, '43210', 'harie', '2018-12-02 05:25:26', '2018-12-02 05:25:26'),
(63, 1, '111111111', 'shyame', '2018-12-02 05:25:26', '2018-12-02 05:25:26'),
(64, 1, '2222222222', NULL, '2018-12-02 05:25:26', '2018-12-02 05:25:26'),
(67, 2, '9861585016110', 'updated67', '2018-12-03 02:24:15', '2018-12-04 04:12:25'),
(69, 2, '12300', 'rame', '2018-12-03 02:44:05', '2018-12-03 02:44:05'),
(70, 2, '43210', 'harie', '2018-12-03 02:44:05', '2018-12-03 02:44:05'),
(71, 2, '111111111', 'shyame', '2018-12-03 02:44:05', '2018-12-03 02:44:05'),
(72, 2, '2222222222', NULL, '2018-12-03 02:44:05', '2018-12-03 02:44:05'),
(73, 2, '987453453', 'rima', '2018-12-03 02:59:42', '2018-12-03 02:59:42'),
(74, 2, '9864488448', 'nima', '2018-12-03 02:59:42', '2018-12-03 02:59:42'),
(75, 2, '987453453', 'rima', '2018-12-03 04:02:56', '2018-12-03 04:02:56'),
(76, 2, '9864488448', 'nima', '2018-12-03 04:02:56', '2018-12-03 04:02:56'),
(79, 1, '9862098764', 'vao', '2018-12-03 04:32:34', '2018-12-03 04:32:34'),
(80, 2, '987453', 'rima', '2018-12-04 00:22:23', '2018-12-04 00:22:23'),
(81, 2, '98448', 'nima', '2018-12-04 00:22:23', '2018-12-04 00:22:23'),
(82, 2, '543545545', 'bibekwa', '2018-12-05 00:36:57', '2018-12-05 00:36:57'),
(83, 2, '9862067745', 'cache', '2018-12-05 00:43:57', '2018-12-05 00:43:57'),
(84, 2, '98765432', NULL, '2018-12-05 00:55:07', '2018-12-05 00:55:07'),
(85, 2, '54854658', NULL, '2018-12-05 00:55:07', '2018-12-05 00:55:07'),
(86, 2, '9854545', NULL, '2018-12-05 00:57:36', '2018-12-05 00:57:36'),
(87, 2, '767677336', NULL, '2018-12-05 00:57:36', '2018-12-05 00:57:36'),
(88, 2, '7684865', NULL, '2018-12-20 04:15:28', '2018-12-20 04:15:28'),
(89, 2, '7385775', NULL, '2018-12-20 04:15:29', '2018-12-20 04:15:29'),
(90, 1, '9843642827', NULL, '2019-02-11 07:32:42', '2019-02-11 07:32:42'),
(91, 1, '9841722659', NULL, '2019-02-11 07:32:42', '2019-02-11 07:32:42'),
(92, 1, '98436428271', 'dmeo1', '2019-02-11 07:35:34', '2019-02-11 07:35:34'),
(93, 1, '98417226159', 'demo2', '2019-02-11 07:35:35', '2019-02-11 07:35:35'),
(94, 4, '98464563', NULL, '2019-02-18 06:01:10', '2019-02-18 06:01:10'),
(95, 4, '985676773', NULL, '2019-02-18 06:01:10', '2019-02-18 06:01:10'),
(96, 4, '986565653', NULL, '2019-02-18 06:01:10', '2019-02-18 06:01:10'),
(97, 4, '9745846585', NULL, '2019-02-18 06:01:10', '2019-02-18 06:01:10'),
(98, 4, '975446343', NULL, '2019-02-18 06:01:10', '2019-02-18 06:01:10'),
(99, 4, '980374347', NULL, '2019-02-18 06:01:10', '2019-02-18 06:01:10'),
(100, 4, '981373554', NULL, '2019-02-18 06:01:10', '2019-02-18 06:01:10'),
(101, 4, '98243644', NULL, '2019-02-18 06:01:10', '2019-02-18 06:01:10');

-- --------------------------------------------------------

--
-- Table structure for table `easy_senderids`
--

CREATE TABLE `easy_senderids` (
  `id` int(10) UNSIGNED NOT NULL,
  `senderid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL COMMENT '1;requested,2;approved,3;disapproved''',
  `gateway_id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `default` int(11) NOT NULL COMMENT '1=defaults,2= normal''',
  `already_exists` int(11) NOT NULL DEFAULT '0' COMMENT '''0=doesn''t exist,1= already exist,2= exist but approved ''',
  `descriptions` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `easy_senderids`
--

INSERT INTO `easy_senderids` (`id`, `senderid`, `user_id`, `status`, `gateway_id`, `operator_id`, `default`, `already_exists`, `descriptions`, `created_at`, `updated_at`) VALUES
(1, 'easy senderid ntc', 13, 2, 1, 3, 1, 0, 'senderid of ntc for test purpose', '2019-03-11 10:29:05', '2019-03-11 10:44:15'),
(2, 'easy senderid ncell', 13, 2, 2, 2, 1, 0, 'senderid of ncell for test purpose', '2019-03-11 10:31:57', '2019-03-11 10:36:27'),
(3, 'eprima ntc', 13, 2, 1, 3, 1, 0, 'senderid of ncell for test purpose', '2019-03-12 03:54:08', '2019-03-12 03:55:09');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gateways`
--

CREATE TABLE `gateways` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hostname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smscid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `gwname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `port` int(10) UNSIGNED NOT NULL,
  `priority` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gateways`
--

INSERT INTO `gateways` (`id`, `username`, `hostname`, `password`, `smscid`, `operator_id`, `gwname`, `port`, `priority`, `created_at`, `updated_at`) VALUES
(1, 'user3234', 'localhost', 'user3234', 'NTC3234', 3, 'NTCGW', 14014, 1, '2019-03-11 06:54:59', '2019-03-11 06:54:59'),
(2, 'bulkuser', 'localhost', 'bulkuser', 'NCELL', 2, 'NCELLGW', 14014, 1, '2019-03-11 06:57:28', '2019-03-11 07:00:44');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(537, 'default', '{\"displayName\":\"App\\\\Jobs\\\\sendqueuesms\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\sendqueuesms\",\"command\":\"O:21:\\\"App\\\\Jobs\\\\sendqueuesms\\\":7:{s:6:\\\"\\u0000*\\u0000job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:13:\\\"Carbon\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2019-01-13 11:23:57.364271\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:3:\\\"UTC\\\";}s:7:\\\"chained\\\";a:0:{}}\"}}', 255, NULL, 1547378671, 1547378671);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2018_10_26_092457_entrust_setup_tables', 2),
(6, '2018_11_28_090849_create_contact_lists_table', 3),
(7, '2018_11_29_080310_create_contact_numbers_table', 4),
(10, '2018_12_09_074630_create_operators_table', 5),
(12, '2018_12_09_111634_create_gateways_table', 6),
(13, '2018_12_10_091602_create_user_access_key', 7),
(15, '2018_12_19_104316_create_send_sms_table', 9),
(16, '2018_12_27_090312_create_balance_transactions_table', 10),
(17, '2019_01_10_065516_create_jobs_table', 11),
(18, '2019_01_13_092916_create_jobs_table', 12),
(19, '2019_01_13_110234_create_failed_jobs_table', 13),
(20, '2019_01_25_070858_create_resellers_clients_table', 14),
(21, '2019_01_25_073730_create_resellers_clients_table', 15),
(22, '2019_01_25_100211_create_verify_users_table', 16),
(23, '2019_01_25_105001_create_user_verifications_table', 17),
(24, '2019_02_19_152009_create_users_balance_table', 18),
(25, '2019_02_19_155202_create_users_balance_table', 19),
(26, '2019_02_19_162053_create_users_balance_table', 20),
(27, '2019_03_11_122815_create_gateways_table', 21),
(28, '2019_03_11_151448_create_easy_senderids_table', 22),
(30, '2019_03_13_171429_create_sms_transactions_table', 23),
(31, '2019_03_15_123938_create_outbox_table', 24);

-- --------------------------------------------------------

--
-- Table structure for table `operators`
--

CREATE TABLE `operators` (
  `id` int(10) UNSIGNED NOT NULL,
  `coverage_id` int(11) DEFAULT NULL,
  `operator_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operator_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operator_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','inactive') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `operators`
--

INSERT INTO `operators` (`id`, `coverage_id`, `operator_name`, `operator_code`, `operator_description`, `price`, `country_code`, `status`, `created_at`, `updated_at`) VALUES
(2, 1, 'NCELL', '648', 'axiata', '2', '977', 'active', '2018-12-09 03:43:54', '2019-02-21 11:20:43'),
(3, 1, 'NTC', '648', 'Nepal Telecom', '2', '977', 'active', '2018-12-09 04:04:04', '2019-03-11 07:09:13');

-- --------------------------------------------------------

--
-- Table structure for table `outbox`
--

CREATE TABLE `outbox` (
  `id` int(10) UNSIGNED NOT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operator` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `admin_state` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `outbox`
--

INSERT INTO `outbox` (`id`, `mobile_number`, `operator`, `status`, `admin_state`, `created_at`, `updated_at`) VALUES
(6, '9861585016', 3, 1, 0, '2019-03-23 16:37:25', NULL),
(7, '9861585010', 3, 1, 0, '2019-03-23 16:37:25', NULL),
(8, '9861585011', 3, 1, 0, '2019-03-23 16:37:25', NULL),
(9, '9801585016', 2, 1, 0, '2019-03-23 16:37:25', NULL),
(10, '9811585016', 2, 1, 0, '2019-03-23 16:37:25', NULL),
(11, '9861585016', 3, 1, 0, '2019-03-23 16:41:20', NULL),
(12, '9861585010', 3, 1, 0, '2019-03-23 16:41:20', NULL),
(13, '9861585011', 3, 1, 0, '2019-03-23 16:41:20', NULL),
(14, '9801585016', 2, 1, 0, '2019-03-23 16:41:21', NULL),
(15, '9811585016', 2, 1, 0, '2019-03-23 16:41:21', NULL),
(16, '9861585016', 3, 1, 0, '2019-03-23 16:48:14', NULL),
(17, '9861585010', 3, 1, 0, '2019-03-23 16:48:14', NULL),
(18, '9861585011', 3, 1, 0, '2019-03-23 16:48:14', NULL),
(19, '9801585016', 2, 1, 0, '2019-03-23 16:48:14', NULL),
(20, '9811585016', 2, 1, 0, '2019-03-23 16:48:14', NULL),
(21, '9861585016', 3, 1, 0, '2019-03-23 16:52:40', NULL),
(22, '9861585010', 3, 1, 0, '2019-03-23 16:52:40', NULL),
(23, '9861585011', 3, 1, 0, '2019-03-23 16:52:40', NULL),
(24, '9801585016', 2, 1, 0, '2019-03-23 16:52:40', NULL),
(25, '9811585016', 2, 1, 0, '2019-03-23 16:52:40', NULL),
(26, '9861585016', 3, 1, 0, '2019-03-23 16:54:35', NULL),
(27, '9861585010', 3, 1, 0, '2019-03-23 16:54:35', NULL),
(28, '9861585011', 3, 1, 0, '2019-03-23 16:54:35', NULL),
(29, '9801585016', 2, 1, 0, '2019-03-23 16:54:35', NULL),
(30, '9811585016', 2, 1, 0, '2019-03-23 16:54:35', NULL),
(31, '9861585016', 3, 1, 0, '2019-04-12 09:05:32', NULL),
(32, '9861585010', 3, 1, 0, '2019-04-12 09:05:33', NULL),
(33, '9861585011', 3, 1, 0, '2019-04-12 09:05:33', NULL),
(34, '9801585016', 2, 1, 0, '2019-04-12 09:05:33', NULL),
(35, '9811585016', 2, 1, 0, '2019-04-12 09:05:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(10, 'list-user', NULL, NULL, '2018-10-26 12:51:20', '2018-10-26 12:51:20'),
(11, 'logout-user', NULL, NULL, '2018-10-26 13:39:45', '2018-10-26 13:39:45'),
(12, 'assign-role', NULL, NULL, '2018-10-28 23:33:02', '2018-10-28 23:33:02'),
(13, 'create-role', NULL, NULL, '2018-10-28 23:33:53', '2018-10-28 23:33:53'),
(14, 'create-permissions', NULL, NULL, '2018-10-28 23:34:06', '2018-10-28 23:34:06'),
(15, 'attach-permissions', NULL, NULL, '2018-10-28 23:34:17', '2018-10-28 23:34:17'),
(16, 'role-create', 'Create Role', 'Create New Role', '2019-01-17 00:23:20', '2019-01-17 00:23:20'),
(17, 'role-list', 'Display Role Listing', 'List All Roles', '2019-01-17 00:23:20', '2019-01-17 00:23:20'),
(18, 'role-update', 'Update Role', 'Update Role Information', '2019-01-17 00:23:20', '2019-01-17 00:23:20'),
(19, 'role-delete', 'Delete Role', 'Delete Role', '2019-01-17 00:23:20', '2019-01-17 00:23:20'),
(20, 'user-list', 'Display User Listing', 'List All Users', '2019-01-17 00:23:20', '2019-01-17 00:23:20'),
(21, 'client-list', 'Display Client Listing', 'List All Clients', '2019-01-17 00:23:20', '2019-01-17 00:23:20'),
(22, 'client-update', 'Update Client', 'Update Client Information', '2019-01-17 00:23:20', '2019-01-17 00:23:20'),
(23, 'client-delete', 'Delete Client', 'Delete Client Information', '2019-01-17 00:23:20', '2019-01-17 00:23:20');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(10, 8),
(10, 11),
(11, 12),
(12, 8),
(13, 8),
(14, 8),
(15, 8);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(8, 'admin', NULL, NULL, '2018-10-26 06:05:11', '2018-10-26 06:05:11'),
(11, 'Reseller', NULL, NULL, '2018-10-26 06:09:36', '2018-10-26 06:09:36'),
(12, 'Client', NULL, NULL, '2018-10-26 06:10:15', '2018-10-26 06:10:15'),
(13, 'user', NULL, NULL, '2018-11-25 04:40:21', '2018-11-25 04:40:21');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(8, 11),
(8, 12),
(9, 8),
(9, 12),
(10, 8),
(11, 8),
(12, 8),
(13, 8),
(13, 11);

-- --------------------------------------------------------

--
-- Table structure for table `send_push`
--

CREATE TABLE `send_push` (
  `sql_id` bigint(20) NOT NULL,
  `momt` enum('MO','MT') DEFAULT NULL,
  `sender` varchar(20) DEFAULT NULL,
  `receiver` varchar(20) NOT NULL,
  `udhdata` blob,
  `msgdata` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `time` bigint(20) DEFAULT NULL,
  `smsc_id` varchar(255) DEFAULT NULL,
  `service` varchar(255) DEFAULT NULL,
  `account` varchar(255) DEFAULT NULL,
  `id` bigint(20) DEFAULT NULL,
  `sms_type` bigint(20) DEFAULT NULL,
  `mclass` bigint(20) DEFAULT NULL,
  `mwi` bigint(20) DEFAULT NULL,
  `coding` bigint(20) DEFAULT NULL,
  `compress` bigint(20) DEFAULT NULL,
  `validity` bigint(20) DEFAULT NULL,
  `deferred` bigint(20) DEFAULT NULL,
  `dlr_mask` bigint(20) DEFAULT NULL,
  `dlr_url` varchar(255) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  `alt_dcs` bigint(20) DEFAULT NULL,
  `rpi` bigint(20) DEFAULT NULL,
  `charset` varchar(255) DEFAULT NULL,
  `boxc_id` varchar(255) DEFAULT NULL,
  `binfo` varchar(255) DEFAULT NULL,
  `meta_data` text,
  `foreign_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `send_push`
--

INSERT INTO `send_push` (`sql_id`, `momt`, `sender`, `receiver`, `udhdata`, `msgdata`, `time`, `smsc_id`, `service`, `account`, `id`, `sms_type`, `mclass`, `mwi`, `coding`, `compress`, `validity`, `deferred`, `dlr_mask`, `dlr_url`, `pid`, `alt_dcs`, `rpi`, `charset`, `boxc_id`, `binfo`, `meta_data`, `foreign_id`) VALUES
(6, 'MT', 'easy senderid ntc', '9861585016', NULL, 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', NULL, 'NTC3234', 'user3234', NULL, NULL, 2, NULL, NULL, 2, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, 'UTF-16BE', NULL, NULL, NULL, NULL),
(7, 'MT', 'easy senderid ntc', '9861585010', NULL, 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', NULL, 'NTC3234', 'user3234', NULL, NULL, 2, NULL, NULL, 2, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, 'UTF-16BE', NULL, NULL, NULL, NULL),
(8, 'MT', 'easy senderid ntc', '9861585011', NULL, 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', NULL, 'NTC3234', 'user3234', NULL, NULL, 2, NULL, NULL, 2, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, 'UTF-16BE', NULL, NULL, NULL, NULL),
(9, 'MT', 'easy senderid ncell', '9801585016', NULL, 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', NULL, 'NCELL', 'bulkuser', NULL, NULL, 2, NULL, NULL, 2, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, 'UTF-16BE', NULL, NULL, NULL, NULL),
(10, 'MT', 'easy senderid ncell', '9811585016', NULL, 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', NULL, 'NCELL', 'bulkuser', NULL, NULL, 2, NULL, NULL, 2, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, 'UTF-16BE', NULL, NULL, NULL, NULL),
(11, 'MT', 'easy senderid ntc', '9861585016', NULL, 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', NULL, 'NTC3234', 'user3234', NULL, NULL, 2, NULL, NULL, 2, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, 'UTF-16BE', NULL, NULL, NULL, NULL),
(12, 'MT', 'easy senderid ntc', '9861585010', NULL, 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', NULL, 'NTC3234', 'user3234', NULL, NULL, 2, NULL, NULL, 2, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, 'UTF-16BE', NULL, NULL, NULL, NULL),
(13, 'MT', 'easy senderid ntc', '9861585011', NULL, 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', NULL, 'NTC3234', 'user3234', NULL, NULL, 2, NULL, NULL, 2, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, 'UTF-16BE', NULL, NULL, NULL, NULL),
(14, 'MT', 'easy senderid ncell', '9801585016', NULL, 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', NULL, 'NCELL', 'bulkuser', NULL, NULL, 2, NULL, NULL, 2, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, 'UTF-16BE', NULL, NULL, NULL, NULL),
(15, 'MT', 'easy senderid ncell', '9811585016', NULL, 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', NULL, 'NCELL', 'bulkuser', NULL, NULL, 2, NULL, NULL, 2, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, 'UTF-16BE', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `send_sms`
--

CREATE TABLE `send_sms` (
  `id` int(10) UNSIGNED NOT NULL,
  `senderid_id` int(11) NOT NULL,
  `contactlist_id` int(11) DEFAULT NULL,
  `contactnumbers_id` int(11) DEFAULT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `send_sms`
--

INSERT INTO `send_sms` (`id`, `senderid_id`, `contactlist_id`, `contactnumbers_id`, `number`, `body`, `created_at`, `updated_at`) VALUES
(127, 1, NULL, NULL, '6356556', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-01-08 01:26:11', '2019-01-08 01:26:11'),
(128, 1, NULL, NULL, '6356556', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-01-08 01:30:51', '2019-01-08 01:30:51'),
(129, 1, NULL, NULL, '6356556', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-01-08 01:33:40', '2019-01-08 01:33:40'),
(130, 1, NULL, NULL, '6356556', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-01-08 01:40:26', '2019-01-08 01:40:26'),
(131, 1, NULL, NULL, '6356556', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-01-08 01:48:05', '2019-01-08 01:48:05'),
(132, 1, NULL, NULL, '6356556', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-01-08 01:49:34', '2019-01-08 01:49:34'),
(133, 1, NULL, NULL, '6356556', 'This is latest sms\nlooking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-01-08 01:51:14', '2019-01-08 01:51:14'),
(134, 1, NULL, NULL, '6356556', 'This is latest sms\nlooking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-01-08 01:52:22', '2019-01-08 01:52:22'),
(135, 1, NULL, NULL, '6356556', 'This is latest sms\nlooking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-01-08 01:53:08', '2019-01-08 01:53:08'),
(136, 1, NULL, NULL, '6356556', 'This is latest sms\nlooking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-01-08 01:55:29', '2019-01-08 01:55:29'),
(137, 1, NULL, NULL, '6356556', 'This is latest sms\nlooking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-01-08 01:56:41', '2019-01-08 01:56:41'),
(141, 1, NULL, NULL, '5488585', 'hello hdjfhkhf gjhdhfgkdhgh ngfdgjlgjfsg ngsdkgdsgdglid jfkrkgrhls fmrfyiyeiyeehrsiuhsue', '2019-01-13 02:25:43', '2019-01-13 02:25:43');

-- --------------------------------------------------------

--
-- Table structure for table `sms_transactions`
--

CREATE TABLE `sms_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `msgtype` int(11) NOT NULL,
  `msgcount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'number of characters in message',
  `cell_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `msg_units` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'number of unit',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sms_transactions`
--

INSERT INTO `sms_transactions` (`id`, `user_id`, `sender`, `message`, `msgtype`, `msgcount`, `cell_no`, `msg_units`, `created_at`, `updated_at`) VALUES
(36, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:37:24', NULL),
(37, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:37:25', NULL),
(38, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:37:25', NULL),
(39, 13, 'easy senderid ncell', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NCELL:1', '2', '2019-03-23 16:37:25', NULL),
(40, 13, 'easy senderid ncell', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NCELL:1', '2', '2019-03-23 16:37:25', NULL),
(41, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:41:20', NULL),
(42, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:41:20', NULL),
(43, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:41:20', NULL),
(44, 13, 'easy senderid ncell', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NCELL:1', '2', '2019-03-23 16:41:20', NULL),
(45, 13, 'easy senderid ncell', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NCELL:1', '2', '2019-03-23 16:41:20', NULL),
(46, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:48:13', NULL),
(47, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:48:13', NULL),
(48, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:48:14', NULL),
(49, 13, 'easy senderid ncell', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NCELL:1', '2', '2019-03-23 16:48:14', NULL),
(50, 13, 'easy senderid ncell', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NCELL:1', '2', '2019-03-23 16:48:14', NULL),
(51, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:52:39', NULL),
(52, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:52:39', NULL),
(53, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:52:40', NULL),
(54, 13, 'easy senderid ncell', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NCELL:1', '2', '2019-03-23 16:52:40', NULL),
(55, 13, 'easy senderid ncell', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NCELL:1', '2', '2019-03-23 16:52:40', NULL),
(56, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:54:35', NULL),
(57, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:54:35', NULL),
(58, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-03-23 16:54:35', NULL),
(59, 13, 'easy senderid ncell', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NCELL:1', '2', '2019-03-23 16:54:35', NULL),
(60, 13, 'easy senderid ncell', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NCELL:1', '2', '2019-03-23 16:54:35', NULL),
(61, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-04-12 09:05:32', NULL),
(62, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-04-12 09:05:32', NULL),
(63, 13, 'easy senderid ntc', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NTC:1', '2', '2019-04-12 09:05:32', NULL),
(64, 13, 'easy senderid ncell', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NCELL:1', '2', '2019-04-12 09:05:32', NULL),
(65, 13, 'easy senderid ncell', 'फ्व्स्फ्व्द्ज्फ्व्द्फ्व्।द्फ्।सेफ्च्।सेफ्च्।सेद्च्।सेद्फ्द्फ्द्फ्फ्फ्चल्', 2, '72', 'NCELL:1', '2', '2019-04-12 09:05:32', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `access_key` longtext COLLATE utf8mb4_unicode_ci,
  `full_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type` enum('1=admin','2=reseller','3=client') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance_type` enum('1=single','2=separated') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT '0',
  `verification_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `access_key`, `full_name`, `organization_name`, `address`, `phone_number`, `mobile_number`, `country`, `user_type`, `balance_type`, `creator_id`, `is_verified`, `verification_token`) VALUES
(1, 'testman', 'test@admi21.com', NULL, '$2y$10$XEU8WxD6uwjbLb1TPl1nZuIfa0NsvgjAGmnDVN9UmtlaXjOllvW.O', NULL, '2018-10-25 23:30:43', '2018-10-25 23:30:43', NULL, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, 0, ''),
(3, 'testman', 'test@admi121.com', NULL, 'password', NULL, '2018-10-26 00:02:35', '2018-10-26 00:02:35', NULL, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, 0, ''),
(8, 'poudelsandip', 'sandippoudel90@gmail.com', NULL, '$2y$10$uNQnMIIbP0h5kj2d3vIOpuGbsSw5va8nDOYkGVdLXRWD/ZOiYRdwu', NULL, '2018-10-26 01:07:09', '2018-10-26 01:07:09', NULL, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, 0, ''),
(9, 'poudelsandip', 'sandippoudel95@gmail.com', NULL, '$2y$10$WDf6NgKQvByAu4yVPFi5nOI8vSzgKX7ZXcZjviVb8QZDM9IEIXPWK', NULL, '2018-10-26 01:10:21', '2018-10-26 01:10:21', NULL, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, 0, ''),
(10, 'poudelsandip', 'sandippoudel97@gmail.com', NULL, '$2y$10$9rBSZH6iYn9CeC9g8bQJROwknOmMnQfTcPjKhJ8ZZmbn.dUwKpQ/y', NULL, '2018-10-26 01:17:44', '2018-10-26 01:17:44', NULL, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, 0, ''),
(11, 'poudelsandip', 'sandippoudel99@gail.com', NULL, '$2y$10$YfKy.ToqApdq7ZSsGjoWBOAgkDA.9rkxAa0KvN50B7r1fT6zm3.iu', NULL, '2018-10-26 01:34:37', '2018-10-26 01:34:37', NULL, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, 0, ''),
(12, 'easy', 'easy@admin.com', NULL, '$2y$10$qwRZ3E5f7tlATUnB8DLJMuCbC7SbAEf7d0ss.uxf/EKl6aH/xC1se', NULL, '2018-10-26 12:42:01', '2018-10-26 12:42:01', NULL, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, 1, ''),
(13, 'easy', 'easys@admin.com', NULL, '$2y$10$z1xPL8oQL0kimA7EFNK5D.dIm/w0JX2UuPBq.cRgVLEU0UTonje/S', NULL, '2018-10-26 13:15:21', '2018-10-26 13:15:21', NULL, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, 1, ''),
(31, 'janta', 'janta@admin.com', NULL, '$2y$10$tqs07DpK/iOt9u6d0uDqGOdRzB9QiIYGURlkQntfeEdfrVb4s1bie', NULL, '2019-01-25 02:05:36', '2019-01-25 02:05:36', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '2=reseller', '1=single', 13, 1, ''),
(32, 'jantakhanal', 'jantakhanal@admin.com', NULL, '$2y$10$kJvs5axOuWvy6dpdbDmLM.pbIYXHi2w/zMLx.RlFQd6jk.WPhHRB6', NULL, '2019-01-25 02:48:33', '2019-01-25 02:48:33', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '2=reseller', '1=single', 13, 0, ''),
(33, 'janta', 'janta@admin1.com', NULL, '$2y$10$nW0iI58L/DGxgNnl2gOsQuLvz6vjsXNlA0kUq.irZV.hg/3To3Gb6', NULL, '2019-01-25 02:55:05', '2019-01-25 02:55:05', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '2=reseller', '1=single', 13, 0, ''),
(34, 'client', 'clientjanta@admin1.com', NULL, '$2y$10$LXTB5vzPeh7qRHKv44vHo.aiAdYC1BaM4EgFDIavXCOe7pFtUGGji', NULL, '2019-01-25 03:01:18', '2019-01-25 03:01:18', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '3=client', '1=single', 13, 0, ''),
(35, 'client', 'clientjanta1@admin1.com', NULL, '$2y$10$dMdm0MCl4KaeM/41eAHbx.K3wJvifszqKtrmO.rLJZ7RGvUtzdROe', NULL, '2019-01-25 03:05:06', '2019-01-25 03:05:06', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '3=client', '1=single', 31, 0, ''),
(36, 'client', 'testemail@admin1.com', NULL, '$2y$10$pc6zHF/xtPeHsfGzLxUA3u246/Mt6Gjykf6BjCs4erMLxG3imXnv.', NULL, '2019-01-25 04:11:10', '2019-01-25 04:11:10', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '3=client', '1=single', 31, 0, ''),
(37, 'client', 'testemail@admin21.com', NULL, '$2y$10$8UH7Hb3VoTmEU8D3ETu9uuBQQHRtfU8eBB3OIaWC2vTvscoAI8AkS', NULL, '2019-01-25 04:13:03', '2019-01-25 04:13:03', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '3=client', '1=single', 31, 0, ''),
(38, 'demo-reseller', 'demoreseller@admin21.com', NULL, '$2y$10$oBnRjXzZON1tUC3.ua2CmuQAQoTrkN7xbDy2MiHLSsBOG4cRXqWdy', NULL, '2019-01-27 03:13:45', '2019-01-27 03:13:45', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '2=reseller', '1=single', 13, 1, 'NdLlmcij2ZJYT6XMUwrMrI5BfP031R'),
(39, 'demo-reseller-client', 'demoresellerclient@admin.com', NULL, '$2y$10$O4NwjcurOlJifmwaD2pUTeILtxSgol7rdsnUduu2vV5/6SjyIRLBy', NULL, '2019-01-27 03:29:42', '2019-01-27 03:29:42', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '2=reseller', '1=single', 13, 0, 'ZVymxGuqmAFpyNBoc3AbmTGPNouWQ1'),
(40, 'updatedname', 'demoresellerclient1@admin.com', '2019-01-27 03:51:28', '$2y$10$4JOO5zjeiNZblF8BVLiTXutwVZuWoO0LMWGVDBCiVU4YGkt7nQSTW', NULL, '2019-01-27 03:32:25', '2019-01-27 03:51:28', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '2=reseller', '1=single', 13, 1, 'XbrL1mK7RLIPwCDuaWBQZjozVwpG8C'),
(41, 'demo-reseller-sandip', 'demoresellersandip@admin.com', NULL, '$2y$10$04sjlzxc.1Q8b7SgdvSem.QIDKqK0.gbyNp73yMcmfi.0kwbMVhey', NULL, '2019-01-27 03:58:31', '2019-01-27 03:58:31', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '2=reseller', '1=single', 13, 0, 'kjaxQt1UhmMKDArFNdod7qgJxF6ZDy'),
(42, 'demo-reseller-poudel', 'demoresellerpoudel@admin.com', '2019-01-27 23:59:03', '$2y$10$YXanemf/iA/eIZXNrH4wAuL4aJ6IWQ9eQXZyu0KIdwzf3WKC197Va', NULL, '2019-01-27 23:57:20', '2019-01-27 23:59:03', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '2=reseller', '1=single', 13, 1, 'K2B6rzjU3sYj5HANLCjUAOlzbImkm4'),
(43, 'demo-reseller-eprima', 'demoresellereprima@admin.com', '2019-01-28 06:03:44', '$2y$10$I9sDcCf2X7cjs/RzAkJeSOv.umbAqnUsHA/sPJpKHj6f2Tjr/h/NO', NULL, '2019-01-28 06:00:11', '2019-01-28 06:03:44', NULL, 'janta khanal', 'demo pvt ltd', 'sankhamul', '2848646844', '654555645', 'nepal', '2=reseller', '1=single', 13, 1, '81LmASa62pEgBYKJ7eTN2KF3AWEdJe');

-- --------------------------------------------------------

--
-- Table structure for table `users_balance`
--

CREATE TABLE `users_balance` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` int(11) DEFAULT NULL,
  `unit` int(11) NOT NULL,
  `balance_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '1=ntc,2=ncell,3=smartcell',
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unitrate` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_balance`
--

INSERT INTO `users_balance` (`id`, `user_id`, `amount`, `unit`, `balance_type`, `description`, `unitrate`, `created_at`, `updated_at`) VALUES
(13, 13, 600, 2, '1', '100 rs added', 1.50, '2019-02-20 09:13:43', '2019-04-12 09:05:32'),
(14, 13, 500, 6, '2', '50 rs added', 1.50, '2019-02-20 09:14:39', '2019-04-12 09:05:32'),
(15, 13, NULL, 60, '3', '100 rs added', 2.00, '2019-02-20 09:15:47', '2019-02-20 11:01:56'),
(16, 12, NULL, 10, '1', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `balance_transactions`
--
ALTER TABLE `balance_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_lists`
--
ALTER TABLE `contact_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_numbers`
--
ALTER TABLE `contact_numbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `easy_senderids`
--
ALTER TABLE `easy_senderids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gateways`
--
ALTER TABLE `gateways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operators`
--
ALTER TABLE `operators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outbox`
--
ALTER TABLE `outbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `send_push`
--
ALTER TABLE `send_push`
  ADD PRIMARY KEY (`sql_id`);

--
-- Indexes for table `send_sms`
--
ALTER TABLE `send_sms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sms_transactions`
--
ALTER TABLE `sms_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_balance`
--
ALTER TABLE `users_balance`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `balance_transactions`
--
ALTER TABLE `balance_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `contact_lists`
--
ALTER TABLE `contact_lists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contact_numbers`
--
ALTER TABLE `contact_numbers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `easy_senderids`
--
ALTER TABLE `easy_senderids`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gateways`
--
ALTER TABLE `gateways`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=538;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `operators`
--
ALTER TABLE `operators`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `outbox`
--
ALTER TABLE `outbox`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `send_push`
--
ALTER TABLE `send_push`
  MODIFY `sql_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `send_sms`
--
ALTER TABLE `send_sms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `sms_transactions`
--
ALTER TABLE `sms_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `users_balance`
--
ALTER TABLE `users_balance`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
