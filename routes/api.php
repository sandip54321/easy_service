<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'v1'], function () {
        //loginuser
        Route::post('login', 'AuthController@authenticate');
        //register,delete user
        Route::group(['middleware' =>['jwt.verify']], function(){
            Route::post('register','AuthController@register');
            Route::delete('deleteuser/{id}','AuthController@deleteUser');
            Route::get('getallusers','AuthController@getallusers');
            Route::get('listusers/{userid}','AuthController@list_users');

        });

        //get user
        Route::group(['middleware' => ['jwt.verify']], function() {
                Route::get('user', 'AuthController@getAuthenticatedUser');

            });
            //update user
        Route::group(['middleware' => ['jwt.verify']], function(){
            Route::put('updateuser/{id}','AuthController@updateUser');
        });

            //logout user
        Route::group(['middleware' => ['jwt.verify']], function(){

                Route::post('logout', 'AuthController@logout');
        });
            //roles and permissions
        Route::group(['middleware' => ['jwt.verify','ability:admin,assign-role|create-role|create-permissions|attach-permissions']], function(){

                Route::post('assign-role', 'AuthController@assignRole');
                Route::post('attach-permission', 'AuthController@attachPermission');
                Route::post('role', 'AuthController@createRole');
                Route::post('permission', 'AuthController@createPermission');
        });
        //refresh

        Route::middleware('jwt.refresh')->get('/token/refresh', 'AuthController@refresh');

        //contactlist crud
        Route::resource('contactlists', 'ContactListController');

        //contactnumbers
        Route::post('insertsinglecontact/{id}','ContactNumberController@insertsingle');
        Route::post('insertmultiplecontact','ContactNumberController@insertmultiple');
        Route::post('importcontact','ContactNumberController@import');
        Route::delete('deletecontact/{contactlistid}/{id}','ContactNumberController@deletecontact');
        Route::get('showallcontacts/{id}','ContactNumberController@showcontacts');
        Route::put('updatecontactnumber/{contactlistid}/{id}','ContactNumberController@updatecontact');

        //operators
        Route::get('getoperators','OperatorController@index')->name('alloperators');
        Route::get('showoperator/{id}','OperatorController@show');
        Route::post('storeoperator','OperatorController@store');
        Route::put('updateoperator/{operator}','OperatorController@update');
        Route::delete('deleteoperator/{operator}','OperatorController@destroy');

        //Gateway
        Route::get('getgateways','GatewayController@index');
        Route::get('showgateway/{id}','GatewayController@show');
        Route::post('storegateway','GatewayController@store');
        Route::put('updategateway/{gateway}','GatewayController@update');
        Route::delete('deletegateway/{gateway}','GatewayController@destroy');

        //senderid_management
        Route::get('getallsenderid','SenderidManagementController@index')->name('allsenderid');
        Route::post('addsenderid','SenderidManagementController@addSenderid');
        Route::get('getsenderid/{id}','SenderidManagementController@showbyId');
        Route::post('approveid/{id}','SenderidManagementController@approveSenderid');
        Route::post('disapproveid/{id}','SenderidManagementController@disapproveId');

        //sendsms
        Route::post('/sendsms','SendSmsController@sendSms');
        Route::post('/postsms/{id}','SendSmsController@sms_service');


        //balance transaction
        Route::post('/addbalance','BalanceTransactionController@balanceaccountopen');
        Route::get('/checkbalance/{id}','BalanceTransactionController@checkbalance');
        Route::put('updatebalance/{balance}','BalanceTransactionController@addbalance');
        Route::put('updateunitrate/{balance}','BalanceTransactionController@updateunitrate');

        //sms report
        Route::get('/smsreport/{id}','ReportController@reportby_Id');
        Route::get('/smsreportbysenderid/{id}','ReportController@reportby_senderId');
        Route::get('/smsreportbyuser/{id}','ReportController@smsreport_by_user');
          Route::get('/smsreportbyoperator/{id}','ReportController@smsreport_by_operator');
          Route::post('/smsreportbydaterange','ReportController@smsreport_by_daterange');
      //queue trial
      Route::post('/trialqueue','EmailController@sendEmail');
      //post to send_push
      Route::post('/send_push','SendPushController@sendSms');
        Route::post('/send_trial','TestSendPushController@sendPush');
    });
