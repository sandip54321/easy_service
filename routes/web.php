<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('user/verify/{verification_code}', 'AuthController@verify_user');
Route::post('/send', 'SendSmsController@sendSms');
Route::get('email', 'EmailController@sendEmail');
Route::post('/trialqueue','EmailController@sendEmail');
